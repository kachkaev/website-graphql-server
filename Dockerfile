FROM node:14.17.6-alpine

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV CHROMIUM_PATH=/usr/bin/chromium-browser
RUN apk add --no-cache chromium

RUN mkdir -p /home/node/Downloads \
  && chown -R node:node /home/node \
  && mkdir -p /app \
  && chown -R node:node /app

WORKDIR /app

COPY package.json yarn.lock /app/

RUN YARN_CACHE_FOLDER=/dev/shm/yarn_cache yarn --production

COPY build /app/build/

USER node

EXPOSE 4000
CMD ["npm", "start"]
