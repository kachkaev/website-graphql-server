import envalid from "envalid";
import { resolve } from "path";

export const env = envalid.cleanEnv(process.env, {
  PATH_TO_PROFILE_INFOS: envalid.str(),
  CHROMIUM_USER_AGENT: envalid.str({
    default:
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36",
  }),
  CHROMIUM_HEADLESS: envalid.bool({
    default: true,
  }),
  CHROMIUM_PATH: envalid.str({
    default: "",
  }),
  CHROMIUM_USER_DATA_DIR: envalid.str({
    default: "",
  }),
  CHROMIUM_VIEWPORT_WIDTH: envalid.num({
    default: 915,
  }),
  CHROMIUM_VIEWPORT_HEIGHT: envalid.num({
    default: 872,
  }),
  DISABLE_INTROSPECTION: envalid.bool({
    default: false,
  }),
  ENDPOINT_PATH: envalid.str({ default: "/" }),
  PORT: envalid.port({ default: 4000 }),
  ENGINE_API_KEY: envalid.str({
    default: "",
    desc: "Apollo Engine API key (https://engine.apollographql.com/)",
  }),

  LINKEDIN_LOGIN: envalid.str(),
  LINKEDIN_PASSWORD: envalid.str(),
  LINKEDIN_LOGIN_VERIFICATION_TIMEOUT: envalid.num({
    default: 90,
    desc: "Number of seconds during which linkedin fetcher will wait for verifyLinkedInLogin mutation to be called",
  }),
  LINKEDIN_PROXY_SERVER: envalid.str({ default: "" }),

  TWITTER_CONSUMER_KEY: envalid.str(),
  TWITTER_CONSUMER_SECRET: envalid.str(),
  TWITTER_ACCESS_TOKEN_KEY: envalid.str(),
  TWITTER_ACCESS_TOKEN_SECRET: envalid.str(),

  FETCH_ON_START: envalid.bool({ default: false }),
  FETCH_SCHEDULE: envalid.str({
    default: "42 04,12,18 * * *", // https://crontab.guru/#42_04,12,18_*_*_*
    desc: "Cron job pattern, see https://www.npmjs.com/package/cron",
  }),
  FETCH_SECURITY_TOKEN: envalid.str({
    desc: "Allow mutations only if this value is provided as securityToken param",
  }),
});

export const resolvedPathToProfileInfos = resolve(
  __dirname,
  "../",
  env.PATH_TO_PROFILE_INFOS,
);
export const resolvedPathToProfileInfoFetchErrors = resolve(
  resolvedPathToProfileInfos,
  "fetch-errors",
);
