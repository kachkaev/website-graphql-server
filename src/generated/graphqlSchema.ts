import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  RegExp: RegExp;
  JSON: Record<string, any>;
};

export type Query = {
  __typename?: 'Query';
  profiles?: Maybe<Array<Profile>>;
  unixTimestamp: Scalars['Int'];
};


export type QueryProfilesArgs = {
  filter?: Maybe<Scalars['RegExp']>;
  onlyWithInfo?: Maybe<Scalars['Boolean']>;
};

export type Profile = {
  __typename?: 'Profile';
  info?: Maybe<Scalars['JSON']>;
  infoFetchedAt?: Maybe<Scalars['Int']>;
  name: Scalars['String'];
  url: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  fetchProfileInfos?: Maybe<FetchProfileInfosPayload>;
  verifyLinkedInLogin?: Maybe<VerifyLinkedInLoginPayload>;
};


export type MutationFetchProfileInfosArgs = {
  input?: Maybe<FetchProfileInfosInput>;
};


export type MutationVerifyLinkedInLoginArgs = {
  input?: Maybe<VerifyLinkedInLoginInput>;
};

export type FetchProfileInfosInput = {
  filter?: Maybe<Scalars['RegExp']>;
  securityToken: Scalars['String'];
};

export type FetchProfileInfosPayload = {
  __typename?: 'FetchProfileInfosPayload';
  results: Array<ProfileInfoFetchResult>;
};

export type ProfileInfoFetchResult = {
  __typename?: 'ProfileInfoFetchResult';
  profile?: Maybe<Profile>;
  success?: Maybe<Scalars['Boolean']>;
};

export type VerifyLinkedInLoginInput = {
  pin: Scalars['Int'];
  securityToken: Scalars['String'];
};

export type VerifyLinkedInLoginPayload = {
  __typename?: 'VerifyLinkedInLoginPayload';
  expected: Scalars['Boolean'];
};

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Query: ResolverTypeWrapper<{}>;
  RegExp: ResolverTypeWrapper<Scalars['RegExp']>;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Profile: ResolverTypeWrapper<Profile>;
  JSON: ResolverTypeWrapper<Scalars['JSON']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  String: ResolverTypeWrapper<Scalars['String']>;
  Mutation: ResolverTypeWrapper<{}>;
  FetchProfileInfosInput: FetchProfileInfosInput;
  FetchProfileInfosPayload: ResolverTypeWrapper<FetchProfileInfosPayload>;
  ProfileInfoFetchResult: ResolverTypeWrapper<ProfileInfoFetchResult>;
  VerifyLinkedInLoginInput: VerifyLinkedInLoginInput;
  VerifyLinkedInLoginPayload: ResolverTypeWrapper<VerifyLinkedInLoginPayload>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Query: {};
  RegExp: Scalars['RegExp'];
  Boolean: Scalars['Boolean'];
  Profile: Profile;
  JSON: Scalars['JSON'];
  Int: Scalars['Int'];
  String: Scalars['String'];
  Mutation: {};
  FetchProfileInfosInput: FetchProfileInfosInput;
  FetchProfileInfosPayload: FetchProfileInfosPayload;
  ProfileInfoFetchResult: ProfileInfoFetchResult;
  VerifyLinkedInLoginInput: VerifyLinkedInLoginInput;
  VerifyLinkedInLoginPayload: VerifyLinkedInLoginPayload;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  profiles?: Resolver<Maybe<Array<ResolversTypes['Profile']>>, ParentType, ContextType, RequireFields<QueryProfilesArgs, never>>;
  unixTimestamp?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
}>;

export interface RegExpScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['RegExp'], any> {
  name: 'RegExp';
}

export type ProfileResolvers<ContextType = any, ParentType extends ResolversParentTypes['Profile'] = ResolversParentTypes['Profile']> = ResolversObject<{
  info?: Resolver<Maybe<ResolversTypes['JSON']>, ParentType, ContextType>;
  infoFetchedAt?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  url?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export interface JsonScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['JSON'], any> {
  name: 'JSON';
}

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
  fetchProfileInfos?: Resolver<Maybe<ResolversTypes['FetchProfileInfosPayload']>, ParentType, ContextType, RequireFields<MutationFetchProfileInfosArgs, never>>;
  verifyLinkedInLogin?: Resolver<Maybe<ResolversTypes['VerifyLinkedInLoginPayload']>, ParentType, ContextType, RequireFields<MutationVerifyLinkedInLoginArgs, never>>;
}>;

export type FetchProfileInfosPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['FetchProfileInfosPayload'] = ResolversParentTypes['FetchProfileInfosPayload']> = ResolversObject<{
  results?: Resolver<Array<ResolversTypes['ProfileInfoFetchResult']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type ProfileInfoFetchResultResolvers<ContextType = any, ParentType extends ResolversParentTypes['ProfileInfoFetchResult'] = ResolversParentTypes['ProfileInfoFetchResult']> = ResolversObject<{
  profile?: Resolver<Maybe<ResolversTypes['Profile']>, ParentType, ContextType>;
  success?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type VerifyLinkedInLoginPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['VerifyLinkedInLoginPayload'] = ResolversParentTypes['VerifyLinkedInLoginPayload']> = ResolversObject<{
  expected?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
  Query?: QueryResolvers<ContextType>;
  RegExp?: GraphQLScalarType;
  Profile?: ProfileResolvers<ContextType>;
  JSON?: GraphQLScalarType;
  Mutation?: MutationResolvers<ContextType>;
  FetchProfileInfosPayload?: FetchProfileInfosPayloadResolvers<ContextType>;
  ProfileInfoFetchResult?: ProfileInfoFetchResultResolvers<ContextType>;
  VerifyLinkedInLoginPayload?: VerifyLinkedInLoginPayloadResolvers<ContextType>;
}>;

