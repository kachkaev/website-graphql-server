export interface Profile {
  url: string;
  fetchInfoTasks?: FetchTask[];
  // fetchInfo?: () => Promise<void>;
  // getInfo?: () => Promise<any>;
  // getInfoFetchedAt?: () => Promise<undefined | number>;
}

export type FetchTaskDefinition = any;
export interface FetchTaskContext {
  errorReportPathWithoutExtension: string;
}
export type FetchTask = (env: FetchTaskContext) => Promise<JSON>;
