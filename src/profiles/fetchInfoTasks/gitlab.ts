import executeJsonFetchTask from "../helpers/fetchTaskExecutors/json";
import { FetchTask } from "../types";

const fetchInfoTasksForGitlab: FetchTask[] = [
  (context) =>
    executeJsonFetchTask(context, {
      url: "https://gitlab.com/api/v4/users/741429/projects",
      collectData: (json) => {
        return {
          repoCount: json.length,
        };
      },
    }),
];

export default fetchInfoTasksForGitlab;
