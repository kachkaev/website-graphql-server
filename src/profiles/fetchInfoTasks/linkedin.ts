import sleep from "sleep-promise";

import { env } from "../../config";
import executeChromiumFetchTask from "../helpers/fetchTaskExecutors/chromium";
import { FetchTask } from "../types";

let lastSubmittedPin: string | undefined;
let pinBeingExpected = false;
export const acquirePin = (pin: string) => {
  lastSubmittedPin = pin;

  return pinBeingExpected;
};

const getManuallySubmittedPin = async () => {
  pinBeingExpected = true;
  lastSubmittedPin = undefined;
  // eslint-disable-next-line no-console
  console.log(
    `Please call verifyLinkedInLogin mutation within ${env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT} sec to complete login`,
  );

  let timeToWait = env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT * 1000;

  while (timeToWait > 0) {
    if (lastSubmittedPin) {
      pinBeingExpected = false;
      // eslint-disable-next-line no-console
      console.log(`Received LinkedIn pin ${lastSubmittedPin}`);

      return lastSubmittedPin;
    }
    timeToWait -= 100;
    await sleep(100);
  }

  // eslint-disable-next-line no-console
  console.log(
    `Linkedin pin was not received within ${env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT} sec`,
  );
  pinBeingExpected = false;
  lastSubmittedPin = undefined;
  throw new Error(
    `Linkedin pin was not received within ${env.LINKEDIN_LOGIN_VERIFICATION_TIMEOUT} sec`,
  );
};

const fetchInfoTasksForLinkedin: FetchTask[] = [
  (context) =>
    executeChromiumFetchTask(context, {
      url: "https://www.linkedin.com/login",
      proxyServer: env.LINKEDIN_PROXY_SERVER,
      collectData: async (page) => {
        await page.focus("input[name=session_key]");
        await page.type(
          //
          "input[name=session_key]",
          env.LINKEDIN_LOGIN,
          {
            delay: 50,
          },
        );
        await page.focus("input[name=session_password]");
        await page.type("input[name=session_password]", env.LINKEDIN_PASSWORD, {
          delay: 50,
        });
        await page.click("button[type=submit]", { delay: 100 });

        await page.waitForNavigation();

        if (!page.url().includes("feed")) {
          const pin = await getManuallySubmittedPin();
          // https://www.linkedin.com/uas/consumer-email-challenge
          await page.focus("#verification-code");
          await page.type("#verification-code", `${pin}`, {
            delay: 50,
          });
          await page.click("#btn-primary", { delay: 100 });
          await page.waitForNavigation();
        }

        await page.goto("https://www.linkedin.com/in/kachkaev/");

        const rawConnectionCount =
          (await (
            await page.$x("//span[contains(., ' connections')]")
          )[0]?.evaluate((el) => el.textContent)) ?? "";

        const connectionCount = parseInt(rawConnectionCount);
        if (!(connectionCount > 0) && rawConnectionCount !== "500+") {
          throw new Error(
            `Failed to extract connection count (${rawConnectionCount} unexpected)`,
          );
        }

        return {
          connectionCount:
            rawConnectionCount === "500+"
              ? rawConnectionCount
              : connectionCount,
        };
      },
    }),
];

export default fetchInfoTasksForLinkedin;
