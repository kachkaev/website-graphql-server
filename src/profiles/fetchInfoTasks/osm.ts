import executeXmlFetchTask from "../helpers/fetchTaskExecutors/xml";
import { FetchTask } from "../types";

const fetchInfoTasksForOsm: FetchTask[] = [
  (context) =>
    executeXmlFetchTask(context, {
      // username -> id mapping via http://whosthat.osmz.ru/
      url: "https://www.openstreetmap.org/api/0.6/user/231451",
      collectData: (data) => {
        const changesetCount = data.osm.user[0].changesets[0].$.count;
        const gpsTraceCount = data.osm.user[0].traces[0].$.count;
        if (!changesetCount || !gpsTraceCount) {
          throw new Error(
            `Expected positive values for changesetCount and gpsTraceCount, ${changesetCount} and ${gpsTraceCount} given`,
          );
        }

        return {
          changesetCount: changesetCount * 1,
          gpsTraceCount: gpsTraceCount * 1,
        };
      },
    }),
];

export default fetchInfoTasksForOsm;
