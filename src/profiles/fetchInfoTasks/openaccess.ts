import executeChromiumFetchTask from "../helpers/fetchTaskExecutors/chromium";
import { FetchTask } from "../types";

const fetchInfoTasksForOpenaccess: FetchTask[] = [
  (context) =>
    executeChromiumFetchTask(context, {
      url: "https://openaccess.city.ac.uk/view/creators/Kachkaev=3AA=2E=3A=3A.html",
      collectData: async (page) => {
        await page.waitForSelector(".ep_view_blurb strong");
        const rawPublicationCount = await page.$eval(
          ".ep_view_blurb strong",
          (el) => el.textContent,
        );
        const publicationCount = parseInt(rawPublicationCount ?? "0");
        if (!(publicationCount > 0)) {
          throw new Error(
            `Failed to extract publication count (${rawPublicationCount} unexpected)`,
          );
        }

        return {
          publicationCount,
        };
      },
    }),
];

export default fetchInfoTasksForOpenaccess;
