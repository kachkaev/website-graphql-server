import _ from "lodash";

import executeJsonFetchTask from "../helpers/fetchTaskExecutors/json";
import { FetchTask } from "../types";

const maxReposPerPage = 100;

const fetchInfoTasksForGithub: FetchTask[] = [
  async (context) => {
    const listOfRepos = [];
    let currentListOfRepos = [];
    let page = 1;
    do {
      await executeJsonFetchTask(context, {
        url: `https://api.github.com/users/kachkaev/repos?page=${page}&per_page=${maxReposPerPage}`,
        collectData: (json) => {
          currentListOfRepos = json;
        },
      });
      listOfRepos.push(...currentListOfRepos);
      page += 1;
    } while (currentListOfRepos.length === maxReposPerPage);
    const sources = _.filter(listOfRepos, ["fork", false]);
    const mostPopularRepo = _.maxBy(listOfRepos, "stargazers_count") || {};
    const languages = _.without(_.uniq(_.map(listOfRepos, "language")), null);

    return {
      repoCount: listOfRepos.length,
      sourceCount: sources.length,
      languageCount: languages.length,
      mostPopularRepoName: mostPopularRepo["name"],
      mostPopularRepoUrl: mostPopularRepo["html_url"],
      mostPopularStarCount: mostPopularRepo["stargazers_count"],
    } as any;
  },
];

export default fetchInfoTasksForGithub;
