import fs from "fs-extra";
import puppeteer from "puppeteer";

import { env } from "../../../config";
import { FetchTaskContext } from "../../types";

interface Options {
  url: string;
  collectData: (page: puppeteer.Page) => Promise<any>;
  proxyServer?: string;
  userDataDir?: string;
}

const executeChromiumFetchTask = async (
  { errorReportPathWithoutExtension }: FetchTaskContext,
  {
    url,
    collectData,
    proxyServer = "",
    userDataDir = env.CHROMIUM_USER_DATA_DIR,
  }: Options,
) => {
  let browser: puppeteer.Browser | undefined;
  let page: puppeteer.Page | undefined;
  let result: any;
  try {
    const args = [
      "--disable-dev-shm-usage",
      "--disable-gpu",
      "--headless",
      "--no-sandbox",
    ];
    if (proxyServer) {
      args.push(`--proxy-server=${proxyServer}`);
    }
    if (userDataDir) {
      await fs.ensureDir(userDataDir);
    }
    browser = await puppeteer.launch({
      args,
      headless: env.CHROMIUM_HEADLESS,
      executablePath: env.CHROMIUM_PATH || undefined,
      userDataDir,
    });

    page = await browser.newPage();
    await page.setUserAgent(env.CHROMIUM_USER_AGENT);
    await page.setViewport({
      width: env.CHROMIUM_VIEWPORT_WIDTH,
      height: env.CHROMIUM_VIEWPORT_HEIGHT,
    });
    await page.goto(url);
    result = await collectData(page);
  } catch (e) {
    await page?.screenshot({ path: `${errorReportPathWithoutExtension}.png` });
    await fs.writeFile(`${errorReportPathWithoutExtension}.txt`, e);
    throw new Error();
  } finally {
    await page?.close();
    await browser?.close();
  }

  return result;
};

export default executeChromiumFetchTask;
