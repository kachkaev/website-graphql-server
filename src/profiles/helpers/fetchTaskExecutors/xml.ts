import { writeFile } from "fs-extra";
import fetch from "node-fetch";
import { parseString } from "xml2js";

import { FetchTaskContext } from "../../types";

// https://github.com/Leonidas-from-XIV/node-xml2js/issues/159#issuecomment-221166979
const xml2json = async (xml) => {
  return new Promise((resolve, reject) => {
    parseString(xml, (err, json) => {
      if (err) {
        reject(err);
      } else {
        resolve(json);
      }
    });
  });
};

const executeXmlFetchTask = async (
  { errorReportPathWithoutExtension }: FetchTaskContext,
  { url, collectData },
) => {
  try {
    const xml = await (await fetch(url)).text();
    const data = await xml2json(xml);

    return collectData(data);
  } catch (e) {
    await writeFile(`${errorReportPathWithoutExtension}.txt`, e);
    throw new Error();
  }
};

export default executeXmlFetchTask;
