import { createApolloFetch } from "apollo-fetch";
import fs from "fs-extra";
import yaml from "js-yaml";
import _ from "lodash";
import { utc } from "moment";
import path from "path";

import {
  env,
  resolvedPathToProfileInfoFetchErrors,
  resolvedPathToProfileInfos,
} from "../../config";
import { FetchTask as FetchInfoTask } from "../types";

const resolvePathToInfo = (profileId: string) =>
  path.resolve(resolvedPathToProfileInfos, `${profileId}.yml`);

export const fetchInfo = async (
  profileKey: string,
  fetchInfoTasks: FetchInfoTask[],
) => {
  if (fetchInfoTasks) {
    let info = {};
    const formattedTime = utc().format("YYYY-MM-DD-HHmmss");
    let i = 0;
    for (const fetchInfoTask of fetchInfoTasks) {
      const newInfo = await fetchInfoTask({
        errorReportPathWithoutExtension: path.resolve(
          resolvedPathToProfileInfoFetchErrors,
          `${formattedTime}-${profileKey}-${i}`,
        ),
      });
      info = { ...info, ...newInfo };
      i += 1;
    }
    await fs.writeFile(resolvePathToInfo(profileKey), yaml.dump(info));
  }
};

export const getInfo = async (
  profileKey: string,
): Promise<JSON | undefined> => {
  try {
    return yaml.load(
      await fs.readFile(resolvePathToInfo(profileKey), "utf8"),
    ) as JSON;
  } catch (e) {
    return undefined;
  }
};

export const getInfoFetchedAt = async (
  profileKey: string,
): Promise<number | undefined> => {
  try {
    const stats = await fs.stat(resolvePathToInfo(profileKey));

    return Math.round(+new Date(stats.mtime) / 1000);
  } catch (e) {
    return undefined;
  }
};

const query = /* GraphQL */ `
  mutation FetchProfileInfos($securityToken: String!) {
    fetchProfileInfos(input: { securityToken: $securityToken }) {
      results {
        profile {
          name
        }
        success
      }
    }
  }
`;

export const executeFetchProfileInfos = async ({ uri, when, terminate }) => {
  try {
    // eslint-disable-next-line no-console
    console.log(`Fetching profiles ${when}...`);

    const apolloFetch = createApolloFetch({ uri });
    const mutationResult = await apolloFetch({
      query,
      variables: {
        securityToken: env.FETCH_SECURITY_TOKEN,
      },
    });

    if (mutationResult.errors) {
      throw new Error(mutationResult.errors[0].toString());
    }
    const profileNamesThatFailed = _.map(
      _.filter(
        _.get(mutationResult, ["data", "fetchProfileInfos", "results"]),
        ["success", false],
      ),
      (r) => _.get(r, ["profile", "name"]),
    );
    // eslint-disable-next-line no-console
    console.log(
      `Fetched profiles ${when}. Unsuccessful: ${
        profileNamesThatFailed.length
          ? profileNamesThatFailed.join(", ")
          : "none"
      }`,
    );
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(`Failed fetching profiles ${when}.`);
    terminate(e);
  }
};
