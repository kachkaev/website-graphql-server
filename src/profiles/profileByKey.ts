import fetchInfoTasksForFlickr from "./fetchInfoTasks/flickr";
import fetchInfoTasksForGithub from "./fetchInfoTasks/github";
import fetchInfoTasksForGitlab from "./fetchInfoTasks/gitlab";
import fetchInfoTasksForLinkedin from "./fetchInfoTasks/linkedin";
import fetchInfoTasksForOpenaccess from "./fetchInfoTasks/openaccess";
import fetchInfoTasksForOsm from "./fetchInfoTasks/osm";
import executeTwitterApiFetchTask from "./helpers/fetchTaskExecutors/twitterApi";
import { Profile } from "./types";

const profileByKey: Record<string, Profile> = {
  "flickr": {
    url: "https://www.flickr.com/photos/kachkaev",
    fetchInfoTasks: fetchInfoTasksForFlickr,
  },
  "github": {
    url: "https://github.com/kachkaev",
    fetchInfoTasks: fetchInfoTasksForGithub,
  },
  "gitlab": {
    url: "https://gitlab.com/kachkaev",
    fetchInfoTasks: fetchInfoTasksForGitlab,
  },
  "linkedin": {
    url: "https://www.linkedin.com/in/kachkaev",
    fetchInfoTasks: fetchInfoTasksForLinkedin,
  },
  "openaccess": {
    url: "https://openaccess.city.ac.uk/view/creators/Kachkaev=3AA=2E=3A=3A.html",
    fetchInfoTasks: fetchInfoTasksForOpenaccess,
  },
  "osm": {
    url: "https://www.openstreetmap.org/user/Kachkaev",
    fetchInfoTasks: fetchInfoTasksForOsm,
  },
  "twitter-en": {
    url: "https://twitter.com/kachkaev",
    fetchInfoTasks: [
      (context) =>
        executeTwitterApiFetchTask(context, {
          userName: "kachkaev",
        }),
    ],
  },
  "twitter-ru": {
    url: "https://twitter.com/kachkaev_ru",
    fetchInfoTasks: [
      (context) =>
        executeTwitterApiFetchTask(context, {
          userName: "kachkaev_ru",
        }),
    ],
  },
  "facebook": {
    url: "https://www.facebook.com/kachkaev",
  },
  "habrahabr": {
    url: "http://habrahabr.ru/users/kachkaev",
  },
  "instagram": {
    url: "https://www.instagram.com/alexanderkachkaev",
  },
  "periscope": {
    url: "https://www.periscope.tv/kachkaev",
  },
  "stackoverflow": {
    url: "https://meta.stackoverflow.com/users/1818285/alexander-kachkaev",
  },
  "vk": {
    url: "https://vk.com/kachkaev",
  },
  "wikipedia-ru": {
    url: "https://ru.wikipedia.org/wiki/User:Kachkaev",
  },
};

export default profileByKey;
