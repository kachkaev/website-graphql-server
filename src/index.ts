import { ApolloServer, gql } from "apollo-server-express";
import { CronJob } from "cron";
import express from "express";
import fs from "fs-extra";
import { resolve } from "path";

import {
  env,
  resolvedPathToProfileInfoFetchErrors,
  resolvedPathToProfileInfos,
} from "./config";
import { executeFetchProfileInfos } from "./profiles";
import resolvers from "./resolvers";

const terminate = (e: Error) => {
  // eslint-disable-next-line no-console
  console.log(e.stack);
  process.exit(1);
};

(async () => {
  const server = new ApolloServer({
    typeDefs: gql(
      fs.readFileSync(resolve(__dirname, "schema.graphql"), "utf8"),
    ),
    resolvers,
    engine: env.ENGINE_API_KEY
      ? {
          apiKey: env.ENGINE_API_KEY,
        }
      : undefined,
    introspection: true,
    playground: true,
  });

  await fs.ensureDir(resolvedPathToProfileInfos);
  await fs.ensureDir(resolvedPathToProfileInfoFetchErrors);

  const app = express();
  app.disable("x-powered-by");
  server.applyMiddleware({ app, path: env.ENDPOINT_PATH });
  app.listen({ port: env.PORT });
  // eslint-disable-next-line no-console
  console.log(
    `GraphQL server ${
      env.ENGINE_API_KEY ? "and Apollo Engine are" : "is"
    } running on port ${env.PORT}. (NODE_ENV=${env.NODE_ENV} ENDPOINT_PATH=${
      env.ENDPOINT_PATH
    })`,
  );

  const uri = `http://localhost:${env.PORT}${env.ENDPOINT_PATH}`;
  if (env.FETCH_ON_START) {
    await executeFetchProfileInfos({
      uri,
      when: "on start",
      terminate,
    });
  }
  if (env.FETCH_SCHEDULE) {
    const job = new CronJob(env.FETCH_SCHEDULE, async () => {
      await executeFetchProfileInfos({
        uri,
        when: `at ${new Date()}`,
        terminate,
      });
    });
    job.start();
  }
})();
