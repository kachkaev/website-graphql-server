import GraphQLJson from "graphql-type-json";
import GraphQLRegExp from "graphql-type-regexp";
import _ from "lodash";

import { env } from "./config";
import { ProfileInfoFetchResult, Resolvers } from "./generated/graphqlSchema";
import { fetchInfo, getInfo, getInfoFetchedAt, profileByKey } from "./profiles";
import { acquirePin } from "./profiles/fetchInfoTasks/linkedin";

const verifySecurityToken = (securityToken: string) => {
  if (securityToken !== env.FETCH_SECURITY_TOKEN) {
    throw new Error("Provided securityToken does not match the expected one");
  }
};

const resolvers: Resolvers = {
  JSON: GraphQLJson,

  RegExp: GraphQLRegExp,

  Profile: {
    info: async ({ name, info }) => {
      if (info) {
        return info;
      }

      return (await getInfo(name)) || null;
    },
    infoFetchedAt: async ({ name }) => (await getInfoFetchedAt(name)) || null,
  },

  Query: {
    unixTimestamp: () => Math.round(+new Date() / 1000),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    profiles: async (__, { filter, onlyWithInfo }) => {
      const matchingProfileKeys = Object.keys(profileByKey).filter(
        (key) => !filter || filter.test(key),
      );
      // https://stackoverflow.com/questions/46619596/apollo-graphql-server-filter-or-sort-by-a-filed-that-is-resolved-separately
      const promises = matchingProfileKeys.map(async (profileKey) => {
        const profile = profileByKey[profileKey]!;

        return {
          name: profileKey,
          url: profile.url,
          info: (await getInfo(profileKey)) || null,
        };
      });
      const results = await Promise.all(promises);
      if (onlyWithInfo) {
        return _.filter(results, (r) => _.isObject(r.info));
      }

      return results;
    },
  },

  Mutation: {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    fetchProfileInfos: async (__, { input }) => {
      const { filter, securityToken } = input || {
        filter: null,
        securityToken: null,
      };
      verifySecurityToken(securityToken || "");
      const matchingProfileKeys = Object.keys(profileByKey).filter(
        (key) =>
          !!profileByKey[key].fetchInfoTasks && (!filter || filter.test(key)),
      );
      const results: ProfileInfoFetchResult[] = [];
      for (const profileKey of matchingProfileKeys) {
        const profile = profileByKey[profileKey]!;
        let success = false;
        if (profile.fetchInfoTasks) {
          try {
            await fetchInfo(profileKey, profile.fetchInfoTasks);
            success = true;
          } catch (e) {
            // success remains as false
          }
        }
        results.push({
          profile: { name: profileKey, url: profile.url },
          success,
        });
      }

      return {
        results,
      };
    },
    // eslint-disable-next-line @typescript-eslint/naming-convention
    verifyLinkedInLogin: async (__, { input }) => {
      const { pin = "", securityToken = "" } = input || {};
      verifySecurityToken(securityToken);
      const expected = await acquirePin(`${pin}`);

      return { expected };
    },
  },
};

export default resolvers;
